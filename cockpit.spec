%define required_base 266
%define _hardened_build 1
%define __lib lib
%if %{defined _pamdir}
%define pamdir %{_pamdir}
%else
%define pamdir %{_libdir}/security
%endif
%bcond_with     pcp

Name:           cockpit
Version:        312
Release:        3
Summary:        A easy-to-use, integrated, glanceable, and open web-based interface for Linux servers
License:        LGPLv2+
URL:            https://cockpit-project.org/
Source0:        https://github.com/cockpit-project/cockpit/releases/download/%{version}/cockpit-%{version}.tar.xz

Patch6000:      backport-CVE-2024-6126.patch

%define build_basic 1
%define build_optional 1

%if "%{name}" == "cockpit"
%define selinuxtype targeted
%define selinux_configure_arg --enable-selinux-policy=%{selinuxtype}
%endif

BuildRequires:  gcc
BuildRequires:  pkgconfig(gio-unix-2.0) pkgconfig(json-glib-1.0) pkgconfig(polkit-agent-1) >= 0.105 pam-devel
BuildRequires:  autoconf automake python3 intltool libssh-devel >= 0.7.1 openssl-devel zlib-devel krb5-devel
BuildRequires:  libxslt-devel docbook-style-xsl glib-networking sed glib2-devel >= 2.50.0
BuildRequires:  systemd-devel krb5-server xmlto gnutls-devel >= 3.6.0
BuildRequires:  gettext >= 0.21 openssh-clients gdb
BuildRequires:  python3-pip
%if %{with pcp}
BuildRequires:  pcp-libs-devel
%endif
BuildRequires:  selinux-policy selinux-policy-devel
Requires:       glib-networking shadow-utils grep libpwquality coreutils NetworkManager kexec-tools openssl glib2 >= 2.50.0
Requires:       python3 python3-dbus systemd udisks2 >= 2.6 PackageKit
Requires:       cockpit-bridge cockpit-ws cockpit-system

Provides:       %{name}-networkmanager %{name}-selinux %{name}-sosreport %{name}-dashboard = %{version}-%{release}
Provides:       %{name}-shell %{name}-systemd
Provides:       %{name}-bridge = %{version}-%{release} %{name}-packagekit = %{version}-%{release} %{name}-storaged = %{version}-%{release}
Provides:       %{name}-system = %{version}-%{release} %{name}-ws = %{version}-%{release} %{name}-ssh %{name}-realmd
Provides:       %{name}-tuned %{name}-users %{name}-kdump
Provides:       bundled(js-jquery) = 3.3.1 bundled(js-moment) = 2.22.2 bundled(nodejs-flot) = 0.8.3 bundled(xstatic-patternfly-common) = 3.35.1
Provides:       bundled(nodejs-promise) = 8.0.2 bundled(nodejs-requirejs) = 2.1.22 bundled(xstatic-bootstrap-datepicker-common) = 1.8.0

Obsoletes:      %{name}-networkmanager %{name}-selinux %{name}-sosreport %{name}-dashboard < %{version}-%{release}
Obsoletes:      %{name}-shell %{name}-systemd
Obsoletes:      %{name}-bridge < %{version}-%{release} %{name}-packagekit < %{version}-%{release} %{name}-storaged < %{version}-%{release}
Obsoletes:      %{name}-system < %{version}-%{release} %{name}-ws < %{version}-%{release} %{name}-ssh %{name}-realmd
Obsoletes:      %{name}-tuned %{name}-users %{name}-kdump

Conflicts:      %{name}-dashboard < 170.x %{name}-ws < 135 firewalld < 0.6.0-1

Recommends:     polkit NetworkManager-team setroubleshoot-server >= 3.3.3 sscg >= 2.3 system-logos
Recommends:     udisks2-lvm2 >= 2.6 udisks2-iscsi >= 2.6 device-mapper-multipath clevis-luks virt-install

%prep
%autosetup -n cockpit-%{version} -p1

%build
%configure \
    %{?selinux_configure_arg} \
    --with-cockpit-user=cockpit-ws \
    --with-cockpit-ws-instance-user=cockpit-wsinstance \
    --with-pamdir='%{pamdir}' \
    --docdir=%_defaultdocdir/%{name} \
%if 0%{?build_basic} == 0
    --disable-ssh \
%endif
%if %{without pcp}
  --disable-pcp
%endif

%make_build

%check
%if %{?_with_check:1}%{!?_with_check:0}
%make_build check
%endif

%install
%make_install
make install-tests DESTDIR=%{buildroot}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pam.d
install -p -m 644 tools/cockpit.pam $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/cockpit
rm -f %{buildroot}/%{_libdir}/cockpit/*.so
install -D -p -m 644 AUTHORS COPYING README.md %{buildroot}%{_docdir}/cockpit/

# Build the package lists for resource packages
# cockpit-bridge is the basic dependency for all cockpit-* packages, so centrally own the page directory
echo '%dir %{_datadir}/cockpit' > base.list
echo '%dir %{_datadir}/cockpit/base1' >> base.list
find %{buildroot}%{_datadir}/cockpit/base1 -type f -o -type l >> base.list
echo '%{_sysconfdir}/cockpit/machines.d' >> base.list
echo %{buildroot}%{_datadir}/polkit-1/actions/org.cockpit-project.cockpit-bridge.policy >> base.list
echo '%dir %{_datadir}/cockpit/ssh' >> base.list

%if %{with pcp}
echo '%dir %{_datadir}/cockpit/pcp' > pcp.list
find %{buildroot}%{_datadir}/cockpit/pcp -type f >> pcp.list
%endif

echo '%dir %{_datadir}/cockpit/shell' >> system.list
find %{buildroot}%{_datadir}/cockpit/shell -type f >> system.list

echo '%dir %{_datadir}/cockpit/systemd' >> system.list
find %{buildroot}%{_datadir}/cockpit/systemd -type f >> system.list

echo '%dir %{_datadir}/cockpit/users' >> system.list
find %{buildroot}%{_datadir}/cockpit/users -type f >> system.list

echo '%dir %{_datadir}/cockpit/metrics' >> system.list
find %{buildroot}%{_datadir}/cockpit/metrics -type f >> system.list

echo '%dir %{_datadir}/cockpit/kdump' > kdump.list
find %{buildroot}%{_datadir}/cockpit/kdump -type f >> kdump.list

echo '%dir %{_datadir}/cockpit/sosreport' > sosreport.list
find %{buildroot}%{_datadir}/cockpit/sosreport -type f >> sosreport.list

echo '%dir %{_datadir}/cockpit/storaged' > storaged.list
find %{buildroot}%{_datadir}/cockpit/storaged -type f >> storaged.list

echo '%dir %{_datadir}/cockpit/networkmanager' > networkmanager.list
find %{buildroot}%{_datadir}/cockpit/networkmanager -type f >> networkmanager.list

echo '%dir %{_datadir}/cockpit/packagekit' > packagekit.list
find %{buildroot}%{_datadir}/cockpit/packagekit -type f >> packagekit.list

echo '%dir %{_datadir}/cockpit/apps' >> packagekit.list
find %{buildroot}%{_datadir}/cockpit/apps -type f >> packagekit.list

echo '%dir %{_datadir}/cockpit/selinux' > selinux.list
find %{buildroot}%{_datadir}/cockpit/selinux -type f >> selinux.list

echo '%dir %{_datadir}/cockpit/playground' > tests.list
find %{buildroot}%{_datadir}/cockpit/playground -type f >> tests.list

echo '%dir %{_datadir}/cockpit/static' > static.list
echo '%dir %{_datadir}/cockpit/static/fonts' >> static.list
find %{buildroot}%{_datadir}/cockpit/static -type f >> static.list

# when not building basic packages, remove their files
%if 0%{?build_basic} == 0
for pkg in base1 branding motd kdump networkmanager selinux shell sosreport ssh static systemd users metrics; do
    rm -r %{buildroot}/%{_datadir}/cockpit/$pkg
    rm -f %{buildroot}/%{_datadir}/metainfo/org.cockpit-project.cockpit-${pkg}.metainfo.xml
done
for data in doc man pixmaps polkit-1; do
    rm -r %{buildroot}/%{_datadir}/$data
done
rm -r %{buildroot}/%{_prefix}/%{__lib}/tmpfiles.d
find %{buildroot}/%{_unitdir}/ -type f ! -name 'cockpit-session*' -delete
for libexec in cockpit-askpass cockpit-session cockpit-ws cockpit-tls cockpit-wsinstance-factory cockpit-client cockpit-client.ui cockpit-desktop cockpit-certificate-helper cockpit-certificate-ensure; do
    rm %{buildroot}/%{_libexecdir}/$libexec
done
rm -r %{buildroot}/%{_sysconfdir}/pam.d %{buildroot}/%{_sysconfdir}/motd.d %{buildroot}/%{_sysconfdir}/issue.d
rm -f %{buildroot}/%{_libdir}/security/pam_*
rm %{buildroot}/usr/bin/cockpit-bridge
rm -f %{buildroot}%{_libexecdir}/cockpit-ssh
rm -f %{buildroot}%{_datadir}/metainfo/cockpit.appdata.xml
%endif

# when not building optional packages, remove their files
%if 0%{?build_optional} == 0
for pkg in apps packagekit playground storaged; do
    rm -rf %{buildroot}/%{_datadir}/cockpit/$pkg
done
# files from -tests
rm -f %{buildroot}/%{pamdir}/mock-pam-conv-mod.so
rm -f %{buildroot}/%{_unitdir}/cockpit-session.socket
rm -f %{buildroot}/%{_unitdir}/cockpit-session@.service
# files from -storaged
rm -f %{buildroot}/%{_prefix}/share/metainfo/org.cockpit-project.cockpit-storaged.metainfo.xml
%endif

sed -i "s|%{buildroot}||" *.list
rm -rf %{buildroot}/usr/src/debug

%description
Cockpit makes GNU/Linux discoverable. See Linux server in a web browser and perform system tasks with a mouse.
It’s easy to start containers, administer storage, configure networks, and inspect logs with this package.

%if %{with pcp}
%package pcp
Summary: Cockpit PCP integration
Requires: cockpit-bridge >= 134.x
Requires: pcp

%description pcp
Cockpit support for reading PCP metrics and loading PCP archives.
%endif

%package devel
Summary:        Test suite for %{name}
Requires:       %{name}-bridge >= 138 %{name}-system >= 138 openssh-clients
Provides:       %{name}-test-assets = %{version}-%{release}
Provides:       %{name}-tests = %{version}-%{release}
Obsoletes:      %{name}-test-assets < 132
Obsoletes:      %{name}-tests < %{version}-%{release}

%description devel
This package contains some test files for testing the %{name}.
It is not necessary for using %{name}.

%package help
Summary:        Help package for %{name}
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}

%description help
This package helps you to deploy %{name} and contains some
man help files.

%pre
getent group cockpit-ws >/dev/null || groupadd -r cockpit-ws
getent passwd cockpit-ws >/dev/null || useradd -r -g cockpit-ws -d /nonexisting -s /sbin/nologin -c "User for cockpit web service" cockpit-ws
getent group cockpit-wsinstance >/dev/null || groupadd -r cockpit-wsinstance
getent passwd cockpit-wsinstance >/dev/null || useradd -r -g cockpit-wsinstance -d /nonexisting -s /sbin/nologin -c "User for cockpit-ws instances" cockpit-wsinstance

if %{_sbindir}/selinuxenabled 2>/dev/null; then
    %selinux_relabel_pre -s %{selinuxtype}
fi

%post
if [ -x %{_sbindir}/selinuxenabled ]; then
    %selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{selinuxtype}/%{name}.pp.bz2
    %selinux_relabel_post -s %{selinuxtype}
fi

%tmpfiles_create cockpit-tempfiles.conf
%systemd_post cockpit.socket cockpit.service
# firewalld only partially picks up changes to its services files without this
test -f %{_bindir}/firewall-cmd && firewall-cmd --reload --quiet || true

# check for deprecated PAM config
if grep --color=auto pam_cockpit_cert %{_sysconfdir}/pam.d/cockpit; then
    echo '**** WARNING:'
    echo '**** WARNING: pam_cockpit_cert is a no-op and will be removed in a'
    echo '**** WARNING: future release; remove it from your /etc/pam.d/cockpit.'
    echo '**** WARNING:'
fi

%preun
%systemd_preun cockpit.socket cockpit.service

%postun
if [ -x %{_sbindir}/selinuxenabled ]; then
    %selinux_modules_uninstall -s %{selinuxtype} %{name}
    %selinux_relabel_post -s %{selinuxtype}
fi
%systemd_postun_with_restart cockpit.socket cockpit.service

%files
%if %{without pcp}
%exclude %{_datadir}/cockpit/pcp/*
%endif
%exclude %{_datadir}/pixmaps/cockpit-sosreport.png
%exclude %{_metainfodir}/{org.cockpit-project.cockpit-sosreport.metainfo.xml,org.cockpit-project.cockpit-kdump.metainfo.xml,org.cockpit-project.cockpit-selinux.metainfo.xml}
%exclude %{_metainfodir}/{org.cockpit-project.cockpit-storaged.metainfo.xml}
%doc AUTHORS COPYING README.md
%dir %{_sysconfdir}/cockpit
%config(noreplace) %{_sysconfdir}/cockpit/ws-certs.d
%config(noreplace) %{_sysconfdir}/pam.d/cockpit
%config %{_sysconfdir}/issue.d/cockpit.issue
%config %{_sysconfdir}/motd.d/cockpit
%ghost %attr(0644, root, root) %{_sysconfdir}/cockpit/disallowed-users
%{_datadir}/metainfo/cockpit.appdata.xml
%{_datadir}/pixmaps/cockpit.png
%{_datadir}/%{name}/motd/{update-motd,inactive.motd}
%{_datadir}/%{name}/{static,branding}
%{_datadir}/%{name}/{base1,ssh,dashboard,realmd,tuned,shell,systemd,users,metrics,kdump,sosreport,storaged,networkmanager,packagekit,apps,machines,ovirt,selinux}/*
%{_unitdir}/{cockpit.service,cockpit-motd.service,cockpit.socket}
%{_unitdir}/cockpit-wsinstance-http.service
%{_unitdir}/cockpit-wsinstance-http.socket
%{_unitdir}/cockpit-wsinstance-https-factory.socket
%{_unitdir}/cockpit-wsinstance-https-factory@.service
%{_unitdir}/cockpit-wsinstance-https@.service
%{_unitdir}/cockpit-wsinstance-https@.socket
%{_unitdir}/system-cockpithttps.slice
%{_sysconfdir}/%{name}/machines.d
%{_prefix}/lib/tmpfiles.d/cockpit-tempfiles.conf
%{_libdir}/security/pam_ssh_add.so
%{_libdir}/security/pam_cockpit_cert.so
%{_libexecdir}/{cockpit-askpass,cockpit-ws,cockpit-ssh,cockpit-wsinstance-factory,cockpit-tls,cockpit-client}
%{_libexecdir}/{cockpit-client.ui,cockpit-desktop,cockpit-certificate-ensure,cockpit-certificate-helper}
%attr(4750, root, cockpit-wsinstance) %{_libexecdir}/cockpit-session
%{_bindir}/cockpit-bridge
%{_datadir}/polkit-1/actions/org.cockpit-project.cockpit-bridge.policy
%dir %{_datadir}/cockpit/shell/images
%{_libdir}/security/mock-pam-conv-mod.so
%{_unitdir}/cockpit-session.socket
%{_unitdir}/cockpit-session@.service
%{_datadir}/metainfo/org.cockpit-project.cockpit-networkmanager.metainfo.xml
%{_datadir}/selinux/packages/%{selinuxtype}/%{name}.pp.bz2
%{_mandir}/man8/%{name}_session_selinux.8cockpit.*
%{_mandir}/man8/%{name}_ws_selinux.8cockpit.*
%{python3_sitelib}/cockpit*

%if %{with pcp}
%files pcp
%{_datadir}/cockpit/pcp/*
%{_libexecdir}/cockpit-pcp
%{_localstatedir}/lib/pcp/config/pmlogconf/tools/cockpit
%endif

%files devel
%{_datadir}/cockpit/playground

%files help
%{_docdir}/cockpit
%exclude %{_docdir}/cockpit/{AUTHORS,COPYING,README.md}
%doc %{_mandir}/man1/{cockpit.1.gz,cockpit-bridge.1.gz,cockpit-desktop.1.gz}
%doc %{_mandir}/man5/cockpit.conf.5.gz
%doc %{_mandir}/man8/{cockpit-ws.8.gz,remotectl.8.gz,pam_ssh_add.8.gz,cockpit-tls.8.gz}

%changelog
* Thu Dec 19 2024 Han Jinpeng <hanjinpeng@kylinos.cn> - 312-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: remove incorrect provides cockpit-machines and cockpit-machines-ovirt
        remove require libvirt and libvirt-client

* Thu Jul 04 2024 lingsheng <lingsheng1@h-partners.com> - 312-2
- Type:CVE
- ID:CVE-2024-6126
- SUG:restart
- DESC:fix CVE-2024-6126

* Tue Mar 05 2024 liweigang <izmirvii@gmail.com> - 312-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:Update to upstream 312 release

* Tue Jan 23 2024 zhouwenpei <zhouwenpei1@h-partners.com> - 309-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:Update to upstream 309 release

* Tue Mar 28 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 285-2
- Type:NA
- ID:NA
- SUG:NA
- DESC:enable selinux to fix cockpit login failed

* Mon Feb 27 2023 lvcongqing <lvcongqing@uniontech.com> - 285-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:Update to upstream 285 release

* Tue Mar 01 2022 herengui <herengui@uniontech.com> - 254-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:Update to upstream 254 release

* Fri Sep 24 2021 wangkerong <wangkerong@huawei.com> - 178-12
- Type:CVE
- Id:CVE-2021-3660
- SUG:NA
- DESC:fix CVE-2021-3660

* Sat Sep 4 2021 hanhui <hanhui15@huawei.com> - 178-11
- strip binary file

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 178-10
- delete gdb in buildrequires

* Wed May 26 2021 liuyumeng <liuyumeng5@huawei.com> - 178-9
- Add a BuildRequires for gcc

* Tue May 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 178-8
- Type:NA
- Id:NA
- SUG:NA
- DESC:increase release number for rebuilding with new libvirt packages

* Sun Jan 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 178-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:bugfix about the confict about cockpit-help and cockpit

* Sun Jan 12 2020 zhangrui <zhangrui182@huawei.com> - 178-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-3804

* Mon Oct 28 2019 caomeng <caomeng5@huawei.com> - 178-5
- Type:NA
- ID:NA
- SUG:NA
- DESC:add bcondwith pcp

* Wed Sep 25 2019 huzhiyu<huzhiyu1@huawei.com> - 178-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix firewalld conflicts

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 178-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add pcp packages for cockpit

* Sat Sep 21 2019 huzhiyu<huzhiyu1@huawei.com> - 178-2
- Package init
